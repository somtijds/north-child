<article <?php post_class('post block-post'); ?>>
	<div class="post-inside">
		<a class="overlay-link" href="<?php the_permalink(); ?>"></a>

		<div class="box-wrap">
			<div class="box-wrap-title">
				<a class="box-title-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				<?php /* echo get_the_term_list( $post->ID, 'categories', '<p></i>', ', ', '</p>' ); */ ?>
			</div>

			<!-- grab the featured image -->
			<?php if ( has_post_thumbnail() ) { ?>
				<a class="featured-image" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'home-image' ); ?></a>
			<?php } ?>

			<!-- add in-progress banner -->
			<?php if ( has_term( 6, 'portfolio_tag', $post->ID ) && ! isset( $in_widget ) ) : ?>
				<div class="in-progress"><span><?php _e( 'In progress', 'north' ); ?><span></div>
			<?php endif; ?>

		</div><!-- box wrap -->
	</div><!-- image post -->
</article><!-- post-->

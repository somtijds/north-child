<?php
/**
 * The template for displaying portfolio posts
 *
 */


	$portfolio_posts_in_progress = new WP_Query(
		array(
			'posts_per_page' => 3,
			'post_type'      => 'array-portfolio',
			'post_status'    => 'publish',
			// Only query the 'work-in-progress' portfolio items first //
			'tax_query'      => array(
				array(
					'taxonomy' => 'portfolio_tag',
					'field' => 'term_id',
					'terms' => array( 6 ),
				),
			),
		)
	);
	var_dump( $portfolio_posts_in_progress );

	global $paged;
	if ( get_query_var( 'paged' ) )
		$paged = get_query_var( 'paged' );
	elseif ( get_query_var( 'page' ) )
		$paged = get_query_var( 'page' );
	else
		$paged = 1;

	$portfolio_posts = new WP_Query(
		array(
			'posts_per_page' => 9,
			'paged'          => $paged,
			'post_type'      => 'array-portfolio'
		)
	);

	if( class_exists( 'Array_Toolkit' ) && $portfolio_posts_in_progress->have_posts() ) : while( $portfolio_posts_in_progress->have_posts() ) : $portfolio_posts_in_progress->the_post();
		include( get_stylesheet_directory() . '/template-portfolio-item.php' );
	endwhile; endif; ?>
	<?php wp_reset_postdata(); ?>

	<?php if( class_exists( 'Array_Toolkit' ) && $portfolio_posts->have_posts() ) : while( $portfolio_posts->have_posts() ) : $portfolio_posts->the_post();
	if ( ! has_term( 6, 'portfolio_tag' ) ) {
		include( get_stylesheet_directory() . '/template-portfolio-item.php' );
	} ?>


<?php endwhile; endif; ?>
<?php wp_reset_postdata(); ?>

<?php north_page_nav( $portfolio_posts ); ?>

<?php
/**
 * @package North Child Theme
 */

/**
 * Load the parent and child theme styles
 */
function north_parent_theme_style() {

	// Parent theme styles
	wp_enqueue_style( 'north-style', get_template_directory_uri(). '/style.css' );

	// Child theme styles
	wp_enqueue_style( 'north-child-style', get_stylesheet_directory_uri(). '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'north_parent_theme_style' );

// function jaap_cohen_category_singular( $plural_string ) {
// 	$plural_string = str_replace( '>Boeken<','>Boek<', $plural_string );
// 	//$plural_string = str_replace( '>Artikelen<','>Artikel<', $plural_string );
// 	return $plural_string;
// }

// function jaap_cohen_filter_portfolio_cat_name( $links ) {
// 	$list_length = count( $links );
// 	if ( count( $list_length ) < 1 ) {
// 		return $links;
// 	}
// 	for ( $i = 0; $list_length > $i; $i++ ) {
// 		$links[$i] = jaap_cohen_category_singular( $links[$i] );
// 	}
// 	return $links;
// }

// add_filter( 'term_links-categories', 'jaap_cohen_filter_portfolio_cat_name' );

// Add child theme trenslations.
// Load translation files from your child theme instead of the parent theme
function jaap_cohen_load_child_theme_locale() {
    load_child_theme_textdomain( 'north', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'jaap_cohen_load_child_theme_locale' );


function jaap_cohen_extra_customizer_css() {
	?>
	<style type="text/css">
		.in-progress {
			background-color: <?php echo get_theme_mod( 'north_link_color', '#48ADD8' ); ?>;
		}
		.in-progress--title {
		 	color: <?php echo get_theme_mod( 'north_link_color', '#48ADD8' ); ?>;
		}
	</style>
	<?php
}
add_action( 'wp_head', 'jaap_cohen_extra_customizer_css' );

// CMB2-functions
require_once( get_stylesheet_directory().'/functions/cmb-functions.php' );

// Load portfolio sidebar widget
require_once( get_stylesheet_directory() . '/includes/widgets/jaap-cohen-recent-portfolio.php' );

function jaap_cohen_get_the_publication_date() {
	$pub_date = get_post_meta( get_the_id(), 'portfolio_item_meta_publication_date', true );
	if ( empty( $pub_date ) ) {
		return;
	}
	//echo mysql2date( 'm/d/Y', $pub_date )
	echo esc_html( $pub_date );
}

function jaap_cohen_get_the_publisher_info() {
	$output = '';
	$publisher = get_post_meta( get_the_id(), 'portfolio_item_meta_publisher', true );
	if ( empty( $publisher ) ) {
		return;
	}
	$publisher_url = get_post_meta( get_the_id(), 'portfolio_item_meta_publisher_url', true );
	if ( ! empty( $publisher_url ) ) {
		$output .= '<a href="' . esc_url( $publisher_url ) . '" target="_blank" title="' . __( "Navigate to the publisher's website", 'north' ) . '">';
	}
	$output .= esc_html( $publisher );
	if ( ! empty( $publisher_url ) ) {
		$output .= '</a>';
	}
	echo $output;
}

function jaap_cohen_get_the_reviews() {
	$output = '';
	$reviews = get_post_meta( get_the_id(), 'portfolio_item_extras_reviews', true );
	if ( empty( $reviews ) ) {
		return;
	}
	$output .= '<ul class="portfolio_item__reviews">';
	foreach ( $reviews as $review ) {
		if ( empty( $review['portfolio_item_review_title'] ) || empty( $review['portfolio_item_review_url'] ) ) {
				continue;
		}
		$output .= '<li><a href="'
			. esc_url( $review['portfolio_item_review_url'] )
			. '" target="_blank" title="' . __( 'Navigate to this review', 'north' )
			. '">'
			. esc_html( $review['portfolio_item_review_title'] )
			. '</a></li>';
	}
	$output .= '</ul>';
	echo $output;
}

function jaap_cohen_get_the_links( $links ) {
	$output = '';
	if ( empty( $links ) ) {
		$links = get_post_meta( get_the_id(), 'portfolio_item_extras_links', true );
	}
	if ( empty( $links ) ) {
		return;
	}
	$output .= '<h2 class="widgettitle">' . __( 'Discover more', 'north' ) . '</h2>';
	$output .= '<ul class="portfolio_item__links">';
	foreach ( $links as $link ) {
		if ( empty( $link['portfolio_item_link_title'] ) || empty( $link['portfolio_item_link_url'] ) ) {
				continue;
		}
		$output .= '<li><a href="'
			. esc_url( $link['portfolio_item_link_url'] )
			. '" target="_blank" title="' . __( 'Open this link', 'north' )
			. '">'
			. esc_html( $link['portfolio_item_link_title'] )
			. '</a></li>';
	}
	$output .= '</ul>';
	echo $output;
}

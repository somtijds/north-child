��    >        S   �      H  
   I  	   T  #   ^  &   �  $   �  F   �  H        ^     o  @   �     �     �     �     �     �     �  
   �     �  0     0   6     g     n  	   |     �     �     �     �     �  +   �  #   �               %     5     8     G  
   W     b     s  	   �     �  
   �     �  
   �     �     �     �     �  
   �  )   �     $	     ;	     Q	     Z	     a	     e	     l	     q	     y	      �	     �	  �  �	  
   �  	   �  &   �  $   �  +   �  Z   "  P   }     �     �  Y   �     J  	   R     \     m  	   r     |  
   �     �     �  $   �     �     �     �     �     �            	     )   %  "   O     r     �     �     �     �     �     �     �     �            	        %  
   -     8     K     \     q     �  *   �     �     �     �     �     �  	                       (     ,                    *              7   0   &           =             6   >   	              3   <             .         $            8   4   %                             #   "   2      ,                          /                 (   !   5   
                )   :   '            9   +   -   ;   1        % Comments 1 Comment <li><span>Next:</span> %title </li> <li><span>Previous:</span> %title</li> Add a url - starting with http(s):// Add a url for a review (or an uploaded PDF) - starting with http(s):// Add an URL to the publication on the publisher's website here (optional) Add another link Add another review Add the publication date here (day of the month will be ignored) Archives Asides Audios By Category Category / %s Category:  Chats Creates links to reviews for this book / article Creates other types of links for portfolio items Day %s Discover more Galleries Images In progress Link {#} Links Month %s Name of the publisher for this book/article Navigate to the publisher's website Navigate to this review No Comments Number to Show: On Open this link Permalink to %s Posted In: Publication Date Publication date: Publisher Publisher URL Publisher: Quotes Recensies: Related items Remove link Remove review Review Title Review {#} Reviews and links for this book / article Search Results for: %s Site Background Color Statuses Title: URL Videos With Year %s field description (optional) monthly archives date formatF Y yearly archives date formatY Project-Id-Version: Jaap Cohen (North Child Theme)
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/style
POT-Creation-Date: 2016-11-17 19:40+0100
PO-Revision-Date: 2016-11-17 20:09+0100
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 % reacties 1 reactie <li><span>Volgende:</span> %title</li> <li><span>Vorige:</span> %title</li> Voeg een URL toe - beginnend met http(s):// Voeg een URL toe naar een recensie (of naar een geüploade PDF) - beginnend met http(s):// Voeg een URL toe voor dit boek/artikel op de website van de uitgever (optioneel) Link toevoegen Recensie toevoegen Vul hier de publicatiedatum in (de dag van de maand zal worden genegeerd in het template) Archief Terzijdes Audio-bijdragen  Door Categorie Categorie / %s Categorie: Chats Links naar recensies Andere links voor dit boek / artikel Dag: %s Meer Fotogalerijen Afbeeldingen In uitvoering Link {#} Links Maand: %s Naam van de uitgever van dit boek/artikel Ga naar de website van de uitgever Ga naar deze recensie Geen reacties Weer te geven aantal: Op Open deze link Permanente link naar %s Geplaatst in: Publicatiedatum Publicatiedatum: Uitgever Uitgever URL Uitgever: Citaten Recensies: Gerelateerde items Link verwijderen Recensie verwijderen Recensie-titel Recensie {#} Recensies en links voor dit boek / artikel Zoekresultaten voor: %s Achtergrondkleur Status-updates Titel: Recensie-URL Video’s Met Jaar: %s Beschrijving F Y Y 
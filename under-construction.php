<!DOCTYPE html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>		 <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>		 <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Binnenkort online: Jaap Cohen, historicus</title>
		<meta property="og:title" content="Jaap Cohen, historicus" />
		<meta property="og:description" content="Hier bouwt Somtijds aan de website voor Jaap Cohen" />
		<meta property="og:url" content="http://www.jaapcohen.nl" />
		<meta property="og:image" content="http://www.jaapcohen.nl/assets/images/pukc-logo-3.png" />
		<meta name="description" content="Binnenkort online: de website van Jaap Cohen, historicus">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">

	</head>
	<body class="noscroll">
		<!--[if lt IE 7]>
			<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		<!-- Add your site or application content here -->
		<div class="uc-wrapper">
			<div class="uc">
				<div class="uc__content">
					<div class="text-wrapper">
						<div class="uc__description">
							<p>Hier bouwt <a href="http://www.somtijds.nl" target="_blank">Somtijds</a> aan een website voor Jaap Cohen, historicus.</p><p>Meer weten?</p>
						</div>
						<p itemscope itemtype="http://data-vocabulary.org/Person">
							<strong>
								<span itemprop="name">Jaap Cohen</span>
							</strong>
							<br />&#9758; <span itemprop="email"><a href="mailto:<?php echo eae_encode_str( 'jaap@jaapcohen.nl' ); ?>"><?php echo eae_encode_str( 'jaap@jaapcohen.nl' ); ?></a></span>
						</p>
					</div><!-- text-wrapper -->
				</div><!-- uc__content -->
			</div><!-- uc -->
		</div><!-- container -->
	</body>
</html>

<?php
/**
 * The template for displaying portfolio category archives
 */
get_header(); ?>

		<header class="page-titles">
			<div class="container clearfix">
				<!-- Page title -->
				<h2 class="entry-title"><?php single_cat_title(); ?></h2>
			</div>
		</header>

		<section class="main">
			<div id="content">
				<div class="container">
					<div class="posts">
						<div class="post-box-wrap clearfix">
							<!-- Grab Portfolio Items -->
							<?php global $wp_query; ?>
							<?php $portfolio_duplicate_query = clone $wp_query; ?>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

								<?php if ( has_term( 6, 'portfolio_tag' ) ) : ?>
									<?php include( get_stylesheet_directory() . '/template-portfolio-item.php' ); ?>
								<?php endif; ?>

							<?php endwhile; ?>
							<?php endif; ?>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

							<?php if ( ! has_term( 6, 'portfolio_tag' ) ) : ?>
								<?php include( get_stylesheet_directory() . '/template-portfolio-item.php' ); ?>
							<?php endif; ?>

							<?php endwhile; ?>
							<?php endif; ?>

							<?php north_page_nav(); ?>

						</div><!-- post box wrap -->
					</div><!-- posts -->
				</div><!-- container -->
			</div><!-- content -->
		</section><!-- main -->

		<!-- footer -->
		<?php get_footer(); ?>

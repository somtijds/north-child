<?php
/**
 * related Portfolio Widget Class
 */
class jaap_cohen_related_portfolio extends WP_Widget {

	/** constructor */
	function jaap_cohen_related_portfolio() {
		parent::__construct( false, $name = __( 'Jaap Cohen Related Portfolio Widget', 'north' ) );
	}

	/** @see WP_Widget::widget */
	function widget( $args, $instance ) {
		if ( ! is_singular( 'array-portfolio' ) ) {
			return;
		}

		extract( $args );
		global $posttypes;
		$title  = apply_filters( 'widget_title', $instance['title'] );
		$number = apply_filters( 'widget_title', $instance['number'] );

		echo $before_widget; ?>

		<?php if ( $title ) echo $before_title . $title . $after_title; ?>

		<?php if ( '' == $title ) { ?>
			<h2 class="widgettitle"><?php _e( 'Related items', 'north' ); ?></h2>
		<?php } ?>

		<div id="related-portfolio-sidebar" class="related-portfolio-sidebar">

			<div class="posts">
				<div class="post-box-wrap clearfix">
					<?php
					$args = array(
						'post_type'      => 'array-portfolio',
						'posts_per_page' => $number,
					);

					global $post;
					$cats = get_the_terms( $post->ID, 'categories' );
					// Remove current portfolio-item from query
					$args['post__not_in'] = array( $post->ID );

					// Add category.
					if ( ! empty( $cats ) && $cats[0] instanceof WP_Term ) {
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'categories',
								'field' => 'term_id',
								'terms' => array( $cats[0]->term_id ),
							),
						);
					}

					$related_portfolio = new WP_Query( $args );

					if( $related_portfolio->have_posts() ) : while($related_portfolio->have_posts() ) : $related_portfolio->the_post();
					$in_widget = true;
					include( get_stylesheet_directory() . '/template-portfolio-item.php' ); ?>


				<!-- <li>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="portfolio-small-image"><?php the_post_thumbnail( 'sidebar-image' ); ?></a>
				</li> -->

			<?php endwhile; endif;
				wp_reset_postdata(); ?>
				</div>
			</div>
		</div>

		<?php echo $after_widget; ?>

		<?php
	}

	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance ) {
		global $posttypes;
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = strip_tags( $new_instance['number'] );
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {

		$posttypes = get_post_types( '', 'objects' );

		$title = esc_attr( $instance['title'] );
		$cat = esc_attr( $instance['cat'] );
		$number = esc_attr( $instance['number'] );
		?>
		 <p>
		  <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', 'north' ); ?></label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
		  <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number to Show:', 'north' ); ?></label>
		  <input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
		</p>
		<?php
	}
}

// Register the widget
function jaap_cohen_register_related_portfolio_widget() {
	register_widget( 'jaap_cohen_related_portfolio' );
}
add_action( 'widgets_init', 'jaap_cohen_register_related_portfolio_widget' );
